package ru.spb.beavers.core.gui;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

import javax.swing.*;
import java.awt.*;

/**
 * Главное окно приложения
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        super("Теория принятия решений");
        this.setMinimumSize(new Dimension(900, 550));
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setContentPane(GUIManager.getContentPane());
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(SyntheticaAluOxideLookAndFeel.class.getName());
            ToolTipManager.sharedInstance().setInitialDelay(300);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                MainFrame mainFrame = new MainFrame();
                mainFrame.setVisible(true);
            }
        });
    }
}
