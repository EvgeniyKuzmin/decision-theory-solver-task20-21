package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI;

import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import org.apache.commons.collections15.Transformer;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.*;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DescriptionPanel
{
    private static Map vx_coords;
    private LastElCoords last_el_coords;
    private ArrayList<Component> graphic_components;



    //////////////////////////////////////////////////////////


    public DescriptionPanel(JPanel panel)
    {
        graphic_components = new ArrayList<Component>();
        last_el_coords = new LastElCoords();

        panel.setLayout(null);
        panel.setSize(760, 2000);
        panel.setPreferredSize(new Dimension(750, 2000));

        // Неформальная постановка задачи
        graphic_components.add(GUIElement.setTextHeader("Неформальная постановка задачи", 20, 300, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 400);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/informal_task_description.html", last_el_coords, panel));

        // Формальная постановка задачи
        graphic_components.add(GUIElement.setTextHeader("Формальная постановка задачи", 30, 300, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 325);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/formal_task_description.html", last_el_coords, panel));

        // Таблица 1.3
        createDescriptionTable(panel);

        // Диаграмма влияния
        graphic_components.add(GUIElement.setTextHeader("Диаграмма влияния", 30, 300, 20, last_el_coords, panel));
        createDependencyDiagram(panel);

        // Описание для диаграммы влияния
        JLabel jl = new JLabel("<html><style> p { text-indent: 20px; }</style><body width=\"580\" height=\"100\">"
                + "<p>1 - Решение о проверке</p>"
                + "<p>2 - Проверка</p>"
                + "<p>3 - Решение о постройке реактора</p>"
                + "<p>4 - Обычный реактор</p>"
                + "<p>5 - Передовой реактор</p>"
                + "<p>6 - Результат</p>"
                + "</body></html>");
        last_el_coords.updateValues(30, 100);
        jl.setSize(600, last_el_coords.height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y);
        graphic_components.add(jl);

        // Дерево решения
        graphic_components.add(GUIElement.setTextHeader("Дерево решений", 30, 300, 20, last_el_coords, panel));
        graphic_components.add(GUIElement.setDescriptionTree(panel, null, last_el_coords, 10));
    }

    public void createDescriptionTable(JPanel panel)
    {
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dm.setDataVector(new Object[][]{
                        {"Обычный", "2", "", "2", "", "4", "10"},
                        {"Передовой", "4", "2", "6", "1", "3", "16"}},
                new Object[]{"Тип реактора", "Стоимость\nпостройки", "небольшой", "крупной", "небольшой", "крупной", "Прибыль"});

        JTable table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
                //renderRight.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMinWidth(100);
        ColumnGroup g_1 = new ColumnGroup("Убытки при аварии");
        g_1.add(cm.getColumn(2));
        g_1.add(cm.getColumn(3));
        ColumnGroup g_2 = new ColumnGroup("Потери жизней при аварии");
        g_2.add(cm.getColumn(4));
        g_2.add(cm.getColumn(5));

        GroupableTableHeader header = (GroupableTableHeader)table.getTableHeader();
        header.addColumnGroup(g_1);
        header.addColumnGroup(g_2);

        table.setPreferredSize(new Dimension(600, table.getRowHeight() * 2));
        table.setSize(new Dimension(600, table.getRowHeight() * 2));

        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(new Dimension(600, 110));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(20, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        graphic_components.add(scroll);

    }

    public void createDependencyDiagram(JPanel panel)
    {
        int ox = 40;
        int oy = 40;

        vx_coords = new HashMap();
        vx_coords.put("1", new Point2D.Double(ox, oy + 20));
        vx_coords.put("2", new Point2D.Double(ox+40, oy+130));
        vx_coords.put("3", new Point2D.Double(ox+70, oy+70));
        vx_coords.put("4", new Point2D.Double(ox+180, oy));
        vx_coords.put("5", new Point2D.Double(ox+150, oy+130));
        vx_coords.put("6", new Point2D.Double(ox+220, oy+60));

        ArrayList<VertexShape> vx_list = new ArrayList<VertexShape>(6);
        vx_list.add(new VertexShape("1", "Square")); // 0
        vx_list.add(new VertexShape("2", "Circle")); // 1
        vx_list.add(new VertexShape("3", "Square")); // 2
        vx_list.add(new VertexShape("4", "Circle")); // 3
        vx_list.add(new VertexShape("5", "Circle")); // 4
        vx_list.add(new VertexShape("6", "Rhomb")); // 5

        Graph<VertexShape, String> basis = new DirectedSparseMultigraph<VertexShape, String>();
        for(VertexShape vx: vx_list)
        {
            basis.addVertex(vx);
        }

        basis.addEdge("1-2", vx_list.get(0), vx_list.get(1));
        basis.addEdge("1-3", vx_list.get(0), vx_list.get(2));
        basis.addEdge("1-6", vx_list.get(0), vx_list.get(5));
        basis.addEdge("2-3", vx_list.get(1), vx_list.get(2));
        basis.addEdge("2-5", vx_list.get(1), vx_list.get(4));
        basis.addEdge("3-6", vx_list.get(2), vx_list.get(5));
        basis.addEdge("4-6", vx_list.get(3), vx_list.get(5));
        basis.addEdge("5-6", vx_list.get(4), vx_list.get(5));

        Transformer<VertexShape, Point2D> locationTransformer = new Transformer<VertexShape, Point2D>() {
            @Override
            public Point2D transform(VertexShape vertex) {
                Point2D vx_coord = (Point2D)vx_coords.get(vertex.name);
                return new Point2D.Double((double) vx_coord.getX(), (double) vx_coord.getY());
            }
        };

        StaticLayout<VertexShape, String> layout = new StaticLayout<VertexShape, String>(
                basis, locationTransformer);
        edu.uci.ics.jung.visualization.VisualizationViewer<VertexShape, String> vv = new edu.uci.ics.jung.visualization.VisualizationViewer<VertexShape, String>(layout);

        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<VertexShape, String>());
        vv.getRenderContext().setVertexLabelTransformer(new Transformer<VertexShape, String>() {
                public String transform(VertexShape vx) {
                    return vx.name;
                }
            }
        );
        vv.getRenderer().setVertexRenderer(new DependencyDiagramRenderer());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.N);
        vv.setSize(315, 190);
        last_el_coords.updateValues(10, vv.getHeight());
        vv.setLocation(panel.getWidth() / 2 - vv.getWidth() / 2, last_el_coords.y);
        graphic_components.add(vv);
    }

    public void fillPanel(JPanel panel)
    {
        panel.setLayout(null);
        panel.setSize(760, 2000);
        panel.setPreferredSize(new Dimension(750, 2000));

        for (Component comp: graphic_components)
        {
            panel.add(comp);
        }
    }
}