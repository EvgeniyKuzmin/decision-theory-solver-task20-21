package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GUIElement;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GroupableTableHeader;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.LastElCoords;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver.*;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.ListenerHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Node;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.INewSolverHandler;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

public class InputPanel extends ListenerHandler<INewSolverHandler>{
    private JComboBox option_list;
    private JComboBox node_1_op_list;
    private JComboBox node_3_2_op_list;
    private JComboBox node_3_3_op_list;
    private JPanel extra_option_panel;
    private JTable table;
    private Node root_node;
    private ConflictSolver solver;
    private LastElCoords last_el_coords;

    private JTextPane strategy_description;

    private Map edge_values;

    //
    private ArrayList<Component> graphic_components;

    public InputPanel(JPanel panel, Node root)
    {
        graphic_components = new ArrayList<Component>();
        last_el_coords = new LastElCoords();

        panel.setLayout(null);
        panel.setSize(760, 1120);
        panel.setPreferredSize(new Dimension(750, 1120));

        createInputDataTable(panel);
        createOptionPanel(panel);

        //root_node = getTree();
        treeInit(root);
        solver = getSolver();
    }

    private boolean isBlockedCell(int row, int column)
    {
        if ((column == 1) && ((row == 0) || (row == 1) || ((row > 3) && (row < 9))))
        {
            return true;
        }
        return false;
    }

    public void setDefualtTableValues()
    {
        table.getModel().setValueAt("-",0,1); table.getModel().setValueAt("34",0,2); table.getModel().setValueAt("232",0,3);
        table.getModel().setValueAt("-",1,1); table.getModel().setValueAt("34",1,2); table.getModel().setValueAt("45",1,3);
        table.getModel().setValueAt("0.3",2,1); table.getModel().setValueAt("345",2,2); table.getModel().setValueAt("1232",2,3);
        table.getModel().setValueAt("0.7",3,1); table.getModel().setValueAt("345",3,2); table.getModel().setValueAt("1232",3,3);
        table.getModel().setValueAt("-",4,1); table.getModel().setValueAt("2323",4,2); table.getModel().setValueAt("2342",4,3);
        table.getModel().setValueAt("-",5,1); table.getModel().setValueAt("233",5,2); table.getModel().setValueAt("12",5,3);
        table.getModel().setValueAt("-",6,1); table.getModel().setValueAt("345",6,2); table.getModel().setValueAt("233",6,3);
        table.getModel().setValueAt("-",7,1); table.getModel().setValueAt("435",7,2); table.getModel().setValueAt("1",7,3);
        table.getModel().setValueAt("-",8,1); table.getModel().setValueAt("3453",8,2); table.getModel().setValueAt("12",8,3);
        table.getModel().setValueAt("0.8",9,1); table.getModel().setValueAt("3453",9,2); table.getModel().setValueAt("4",9,3);
        table.getModel().setValueAt("0.2",10,1); table.getModel().setValueAt("3453",10,2); table.getModel().setValueAt("34",10,3);
        table.getModel().setValueAt("0.6",11,1); table.getModel().setValueAt("345",11,2); table.getModel().setValueAt("45",11,3);
        table.getModel().setValueAt("0.4",12,1); table.getModel().setValueAt("3453",12,2); table.getModel().setValueAt("56",12,3);
        table.getModel().setValueAt("0.2",13,1); table.getModel().setValueAt("3453",13,2); table.getModel().setValueAt("67",13,3);
        table.getModel().setValueAt("0.3",14,1); table.getModel().setValueAt("345",14,2); table.getModel().setValueAt("78",14,3);
        table.getModel().setValueAt("0.5",15,1); table.getModel().setValueAt("56756",15,2); table.getModel().setValueAt("78",15,3);
        table.getModel().setValueAt("0.5",16,1); table.getModel().setValueAt("567",16,2); table.getModel().setValueAt("45",16,3);
        table.getModel().setValueAt("0.5",17,1); table.getModel().setValueAt("5675",17,2); table.getModel().setValueAt("56",17,3);
        table.getModel().setValueAt("0.5",18,1); table.getModel().setValueAt("78978",18,2); table.getModel().setValueAt("565",18,3);
        table.getModel().setValueAt("0.3",19,1); table.getModel().setValueAt("789",19,2); table.getModel().setValueAt("232",19,3);
        table.getModel().setValueAt("0.2",20,1); table.getModel().setValueAt("789",20,2); table.getModel().setValueAt("787",20,3);
    }

    private void createInputDataTable(JPanel panel)
    {
        graphic_components.add(GUIElement.setTextHeader("Введите вероятности и ценности:", 20, 400, 20, last_el_coords, panel));
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return ((column == 0) || (isBlockedCell(row, column))) ? false : true;
            }
        };
        dm.setDataVector(new Object[][]{
                        {"2", "-", "34", "232"},
                        {"3.1", "-", "34", "45"},
                        {"3.2",  "0.3", "345", "1232"},
                        {"3.3", "0.7", "345", "1232"},
                        {"4.1", "-", "2323", "2342"},
                        {"4.2", "-", "233", "12"},
                        {"5.1", "-", "345", "234"},
                        {"4.3", "-", "435", "1"},
                        {"5.2",  "-", "3453", "12"},
                        {"6.1", "0.8", "3453", "4"},
                        {"6.2", "0.2", "3453", "34"},
                        {"6.3", "0.6", "345", "45"},
                        {"6.4", "0.4", "3453", "56"},
                        {"6.5", "0.2", "3453", "67"},
                        {"6.6", "0.3", "345", "78"},
                        {"6.7", "0.5", "56756", "78"},
                        {"6.8", "0.5", "567", "45"},
                        {"6.9", "0.5", "5675", "56"},
                        {"6.10", "0.5", "78978", "565"},
                        {"6.11", "0.3", "789", "232"},
                        {"6.12", "0.2", "789", "787"},
                },
                new Object[]{"Номер\nузла", "Вероятность", "Доход", "Кол-во\nжертв"});
        table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        table.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {
                    Node node = root_node.getChildRecursively(table.getValueAt(e.getFirstRow(), 0).toString());
                    if (node == null)
                        throw new RuntimeException("Runtime: " + table.getValueAt(e.getFirstRow(), 0).toString());
                    switch (e.getColumn()) {
                        case 1:
                            if (table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString().compareTo("-") == 0) {
                                node.getEdgeToParent().setProbability(0.0);
                            }
                            else {
                                node.getEdgeToParent().setProbability(Double.parseDouble(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()));
                            }
                            break;
                        case 2:
                            node.getEdgeToParent().getValue().setV(Double.parseDouble(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()));
                            break;
                        case 3:
                            node.getEdgeToParent().getValue().setN(Double.parseDouble(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()));
                            break;
                        default:
                            throw new RuntimeException("Че-то пошло не так");
                    }
                }
            }
        });
        TableColumnModel cm = table.getColumnModel();

        table.setPreferredSize(new Dimension(600, table.getRowHeight() * 21));
        table.setSize(new Dimension(600, table.getRowHeight() * 21 + 60));

        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(new Dimension(600, 399));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        graphic_components.add(scroll);
    }

    private void createOptionPanel(JPanel panel)
    {
        last_el_coords.updateValues(20, 620);
        JPanel option_panel = new JPanel();
        option_panel.setPreferredSize(new Dimension(580, last_el_coords.height-10));
        option_panel.setSize(600, last_el_coords.height);
        option_panel.setLayout(null);

        JScrollPane scroll = new JScrollPane(option_panel);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setSize(new Dimension(600, last_el_coords.height));
        scroll.setLocation(panel.getWidth() / 2 - scroll.getWidth() / 2, last_el_coords.y);

        JLabel jl = new JLabel("Стратегия:");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(90, 20);

        option_panel.add(jl);

        String[] options = { "Default", "Random", "W Const", "W Var"};

        option_list = new JComboBox(options);
        option_list.setSize(100, 20);
        option_list.setSelectedIndex(1);
        option_list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                solver = getSolver();
                switch (option_list.getSelectedIndex()) {
                    case 0:
                        for (int i = 0; i < 6; i++) {
                            extra_option_panel.getComponent(i).setVisible(true);
                        }
                        for (int i = 6; i < 14; i++) {
                            extra_option_panel.getComponent(i).setVisible(false);
                        }
                        GUIElement.changeHtmlPage("./../html/default_mode.html", strategy_description);
                        break;
                    case 1:
                        for (int i = 0; i < 14; i++) {
                            extra_option_panel.getComponent(i).setVisible(false);
                        }
                        GUIElement.changeHtmlPage("./../html/random_mode.html", strategy_description);
                        break;
                    case 2:
                        for (int i = 0; i < 6; i++) {
                            extra_option_panel.getComponent(i).setVisible(false);
                        }
                        extra_option_panel.getComponent(6).setVisible(true);
                        extra_option_panel.getComponent(7).setVisible(true);
                        for (int i = 8; i < 14; i++) {
                            extra_option_panel.getComponent(i).setVisible(false);
                        }
                        GUIElement.changeHtmlPage("./../html/w_const_mode.html", strategy_description);
                        break;
                    case 3:
                        for (int i = 0; i < 8; i++) {
                            extra_option_panel.getComponent(i).setVisible(false);
                        }
                        for (int i = 8; i < 14; i++) {
                            extra_option_panel.getComponent(i).setVisible(true);
                        }
                        GUIElement.changeHtmlPage("./../html/w_var_mode.html", strategy_description);
                        break;
                }
                for (INewSolverHandler handler : getHandlersList()) {
                    handler.newSolver(solver);
                }
            }
        });
        option_panel.add(option_list);
        jl.setLocation(option_panel.getWidth() / 2 - (jl.getWidth() + option_list.getWidth()) / 2, 20);
        option_list.setLocation(jl.getX() + jl.getWidth(), 20);

        jl = new JLabel("Параметры стратегии:");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(180, 20);
        jl.setLocation(option_panel.getWidth() / 2 - jl.getWidth() / 2,
                option_list.getY() + option_list.getHeight() + 20);
        option_panel.add(jl);

        extra_option_panel = new JPanel();
        extra_option_panel.setPreferredSize(new Dimension(480, 140));
        extra_option_panel.setSize(500, 150);
        extra_option_panel.setLayout(null);

        createExtraOptionPanel();

        JScrollPane scroll2 = new JScrollPane(extra_option_panel);
        scroll2.setSize(new Dimension(500, 150));
        scroll2.setLocation(option_panel.getWidth() / 2 - scroll2.getWidth() / 2,
                jl.getY() + jl.getHeight() + 10);
        scroll2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        option_panel.add(scroll2);

        jl = new JLabel("Описание стратегии:");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(180, 20);
        jl.setLocation(option_panel.getWidth() / 2 - jl.getWidth() / 2, scroll2.getY() + scroll2.getHeight() + 20);
        option_panel.add(jl);

        JScrollPane strategy_pane = GUIElement.setHtmlText("./../html/random_mode.html", option_panel.getWidth() / 2 - 250,
                jl.getY() + jl.getHeight() + 10, 500, 300, option_panel);
        strategy_description = (JTextPane)strategy_pane.getViewport().getView();
        option_panel.add(strategy_pane);
        graphic_components.add(scroll);
    }

    private void createExtraOptionPanel()
    {
        Font font = new Font("Arial", Font.BOLD, 15);

        JLabel jl = new JLabel("Из узла 1 надо переходить в узел ");
        jl.setFont(font);
        jl.setSize(270, 20);
        jl.setLocation(20, 10);
        extra_option_panel.add(jl);

        String[] node_1_op = { "3.1", "2"};
        node_1_op_list = new JComboBox(node_1_op);
        node_1_op_list.setSize(100, 20);
        node_1_op_list.setLocation(jl.getX() + jl.getWidth(), 10);
        node_1_op_list.setSelectedIndex(0);
        node_1_op_list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch (node_1_op_list.getSelectedIndex()) {
                    case 0:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("2");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("3.1");
                        break;
                    case 1:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("3.1");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("2");
                        break;
                }
            }
        });
        extra_option_panel.add(node_1_op_list);

        jl = new JLabel("Из узла 3.2 надо переходить в узел ");
        jl.setFont(font);
        jl.setSize(270, 20);
        jl.setLocation(20, 40);
        extra_option_panel.add(jl);

        String[] node_3_2_op = { "4.2", "5.1"};
        node_3_2_op_list = new JComboBox(node_3_2_op);
        node_3_2_op_list.setSize(100, 20);
        node_3_2_op_list.setLocation(jl.getX() + jl.getWidth(), 40);
        node_3_2_op_list.setSelectedIndex(0);
        node_3_2_op_list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch (node_3_2_op_list.getSelectedIndex()) {
                    case 0:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("5.1");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("4.2");
                        break;
                    case 1:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("4.2");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("5.1");
                        break;
                }
            }
        });
        extra_option_panel.add(node_3_2_op_list);

        jl = new JLabel("Из узла 3.3 надо переходить в узел ");
        jl.setFont(font);
        jl.setSize(270, 20);
        jl.setLocation(20, 70);
        extra_option_panel.add(jl);

        String[] node_3_3_op = { "4.3", "5.2"};
        node_3_3_op_list = new JComboBox(node_3_3_op);
        node_3_3_op_list.setSize(100, 20);
        node_3_3_op_list.setLocation(jl.getX()+jl.getWidth(), 70);
        node_3_3_op_list.setSelectedIndex(0);
        node_3_3_op_list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch (node_3_3_op_list.getSelectedIndex()) {
                    case 0:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("5.2");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("4.3");
                        break;
                    case 1:
                        ((StandartPreferenceConflictSolver) solver).deleteFromPreferred("4.3");
                        ((StandartPreferenceConflictSolver) solver).addToPreferred("5.2");
                        break;
                }
            }
        });
        extra_option_panel.add(node_3_3_op_list);

        jl = new JLabel("w:");
        jl.setFont(font);
        jl.setSize(60, 20);
        jl.setLocation(20, 10);
        extra_option_panel.add(jl);
        SpinnerModel model = new SpinnerNumberModel(0.0, -35000.0, 35000.0, 0.01);
        JSpinner spinner = new JSpinner(model);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ((ConstantWConflictSolver) solver).setW(Double.parseDouble(((JSpinner) e.getSource()).getValue().toString()));
            }
        });
        spinner.setSize(100, 20);
        spinner.setLocation(jl.getX() + jl.getWidth(), 10);
        extra_option_panel.add(spinner);

        jl = new JLabel("r min:");
        jl.setFont(font);
        jl.setSize(60, 20);
        jl.setLocation(20, 10);
        extra_option_panel.add(jl);
        model = new SpinnerNumberModel(0.0, -35000.0, 35000.0, 0.01);
        spinner = new JSpinner(model);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ((DynamicWConflictSolver) solver).setRMin(Double.parseDouble(((JSpinner) e.getSource()).getValue().toString()));
            }
        });
        spinner.setSize(100, 20);
        spinner.setLocation(jl.getX() + jl.getWidth(), 10);
        extra_option_panel.add(spinner);

        jl = new JLabel("r max:");
        jl.setFont(font);
        jl.setSize(60, 20);
        jl.setLocation(20, 40);
        extra_option_panel.add(jl);
        model = new SpinnerNumberModel(0.0, -35000.0, 35000.0, 0.01);
        spinner = new JSpinner(model);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ((DynamicWConflictSolver) solver).setRMax(Double.parseDouble(((JSpinner) e.getSource()).getValue().toString()));
            }
        });
        spinner.setSize(100, 20);
        spinner.setLocation(jl.getX() + jl.getWidth(), 40);
        extra_option_panel.add(spinner);

        jl = new JLabel("B:");
        jl.setFont(font);
        jl.setSize(60, 20);
        jl.setLocation(20, 70);
        extra_option_panel.add(jl);
        model = new SpinnerNumberModel(1, 1, 10, 1);
        spinner = new JSpinner(model);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ((DynamicWConflictSolver) solver).setBeta(Double.parseDouble(((JSpinner) e.getSource()).getValue().toString()));
            }
        });
        spinner.setSize(100, 20);
        spinner.setLocation(jl.getX() + jl.getWidth(), 70);
        extra_option_panel.add(spinner);

        for (int i = 0; i < 14; i++) {
            extra_option_panel.getComponent(i).setVisible(false);
        }
    }

    public void readFromFile(File file)
    {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(file.getPath()), Charset.forName("UTF-8"))) {
            String line = null;
            for (int i = 0;((line = reader.readLine()) != null); ++i) {
                table.getModel().setValueAt(line.split(" ")[0], i, 1);
                table.getModel().setValueAt(line.split(" ")[1], i, 2);
                table.getModel().setValueAt(line.split(" ")[2], i, 3);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    public void writeToFile(File file)
    {
        String line = "";
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file.getPath()), Charset.forName("UTF-8"))) {
            if (table.getRowCount() != 0)
            {
                line = table.getModel().getValueAt(0, 1).toString() + " "
                        + table.getModel().getValueAt(0, 2).toString() + " "
                        + table.getModel().getValueAt(0, 3).toString();
                writer.write(line, 0, line.length());
                for (int i = 1; i < table.getRowCount(); ++i)
                {
                    line = table.getModel().getValueAt(i, 1).toString() + " "
                            + table.getModel().getValueAt(i, 2).toString() + " "
                            + table.getModel().getValueAt(i, 3).toString();
                    writer.newLine();
                    writer.write(line, 0, line.length());
                }
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    private void treeInit(Node node)
    {
        root_node = node;
        ArrayList<Node> node_stack = new ArrayList<Node>();
        node_stack.add(root_node);
        while (!node_stack.isEmpty())
        {
            Node current_root_node = node_stack.remove(0);
            for (Node child: current_root_node.getChilds())
            {
                node_stack.add(child);
            }
            for (int i = 0; i < table.getRowCount(); ++i)
            {
                if (table.getModel().getValueAt(i,0).toString().equals(current_root_node.getName()))
                {
                    if (table.getModel().getValueAt(i, 1).toString().compareTo("-") == 0) {
                        current_root_node.getEdgeToParent().setProbability(0.0);
                    }
                    else {
                        current_root_node.getEdgeToParent().setProbability(Double.parseDouble((String) table.getModel().getValueAt(i, 1)));
                    }
                    current_root_node.getEdgeToParent().getValue().setV(Double.parseDouble((String)table.getModel().getValueAt(i, 2)));
                    current_root_node.getEdgeToParent().getValue().setN(Double.parseDouble((String) table.getModel().getValueAt(i, 3)));
                    break;
                }
            }
        }
    }

    private ConflictSolver getSolver(){
        if ((option_list.getSelectedItem()).equals("Default")) {
            StandartPreferenceConflictSolver solver = new StandartPreferenceConflictSolver();
            solver.addToPreferred((String)node_1_op_list.getSelectedItem());
            solver.addToPreferred((String)node_3_2_op_list.getSelectedItem());
            solver.addToPreferred((String)node_3_3_op_list.getSelectedItem());
            return solver;
        }else if ((option_list.getSelectedItem()).equals("Random")) {
            return new RandomConflictSolver();
        }else if ((option_list.getSelectedItem()).equals("W Const")) {
            return new ConstantWConflictSolver(Double.parseDouble(((JSpinner)extra_option_panel.getComponent(7)).getModel().getValue().toString()));
        }else if ((option_list.getSelectedItem()).equals("W Var")) {
            return new DynamicWConflictSolver(
                    Double.parseDouble(((JSpinner) extra_option_panel.getComponent(13)).getModel().getValue().toString())
                    , Double.parseDouble(((JSpinner) extra_option_panel.getComponent(9)).getModel().getValue().toString())
                    , Double.parseDouble(((JSpinner) extra_option_panel.getComponent(11)).getModel().getValue().toString())
            );
        } else {
            throw new RuntimeException("df");
        }
    }

    public void fillPanel(JPanel panel)
    {
        panel.setLayout(null);
        panel.setSize(760, 1120);
        panel.setPreferredSize(new Dimension(750, 1120));

        for (Component comp: graphic_components)
        {
            panel.add(comp);
        }
    }
}