package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Value;

public interface IValueChangeHandler {
    public void valueChanged(Value value);
}
