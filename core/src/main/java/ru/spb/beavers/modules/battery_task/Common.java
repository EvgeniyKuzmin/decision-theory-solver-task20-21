package ru.spb.beavers.modules.battery_task;

import java.awt.*;

/**
 * Клас является хранилищем используемых в модуле BatteryTaskModule констант
 * и предоставляет к ним глобальную точку доступа
 *
 * @author Alexey Borisov
 */
public class Common {

	public static final String TITLE = "2.1.1 Задача о батареях";
	public static final String RESOURCE_PATH = "./ru/spb/beavers/modules/battery_task/";
	public static final String DESCRIPTION_FILE = "description.res";
	public static final String SOLUTION_FILE = "solution.res";
	public static final String DIAGRAM_FILE = "diagram.png";
	public static final String BATTERIES_COST = "<html>Себестоимость производства одной батареи - v<sub>c</sub>:</html>";
	public static final String BATTERIES_PRICE = "<html>Цена одной батареи - v<sub>s</sub>:</html>";
	public static final String INPUT_HEADER = "<html><h2>Исходные данные для решения задачи</h2></html>";
	public static final String DIAGRAM_TEXT = "<html><br/>Диаграмма влияния:<br/><br/></html>";
	public static final String DESCRIPTION_ERROR_TEXT = "<html><h2>Не удалось загрузить описание</h2></html>";
	public static final String SOLUTION_ERROR_TEXT = "<html><h2>Не удалось загрузить решение</h2></html>";
	public static final String PROBABILITY_0 = "<html>Вероятность отсутствия спроса - p<sub>0</sub>:</html>";
	public static final String PROBABILITY_1 = "<html>Вероятность продажи одной батареи - p<sub>1</sub>:</html>";
	public static final String PROBABILITY_2 = "<html>Вероятность продажи двух батарей - p<sub>2</sub>:</html>";
	public static final String PROBABILITY_3 = "<html>Вероятность продажи трех батарей - p<sub>3</sub>:</html>";
	public static final String INPUT_ERROR = "<html><h2><font color='#FF9B9B'>Допускается ввод только чисел</font></h2></html>";
	public static final String PARMS_ERROR = "<html><h2><font color='#FF9B9B'>Исходные данные не полны либо содержат ошибки</font></h2></html>";
	public static final Dimension WORK_SIZE = new Dimension(825, 390);

}