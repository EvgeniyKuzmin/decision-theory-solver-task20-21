package ru.spb.beavers.modules.half_order.graph.matrix.operations;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import edu.uci.ics.jung.algorithms.matrix.GraphMatrixOperations;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

import java.util.Map;

/**
 * Created by Владимир on 09.04.2015.
 */
public class GraphMatrixConverter {
    public static SparseDoubleMatrix2D graphToMatrixConvert(DirectedSparseGraph<String, String> graph){
        return GraphMatrixOperations.graphToSparseMatrix(graph);
    }
    public static DirectedSparseGraph<String, String> matrixToGraphConvert(DoubleMatrix2D matrix, Map<Integer,String> indexer){
        DirectedSparseGraph<String, String> graph = new DirectedSparseGraph<>();
        for (String vertex : indexer.values()) {
            graph.addVertex(vertex);
        }
        if (matrix.rows() != matrix.columns())
        {
            throw new IllegalArgumentException("Matrix must be square.");
        }

        int size = matrix.rows();
        String edge = "edge";
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                double value = matrix.getQuick(i, j);
                if(value == 1.0){
                    graph.addEdge(edge+" "+(j)+" "+(i), indexer.get(i), indexer.get(j));
                }
            }
        }
        return graph;
    }
}
